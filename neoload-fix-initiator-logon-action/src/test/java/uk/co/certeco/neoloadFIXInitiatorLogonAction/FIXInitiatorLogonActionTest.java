package uk.co.certeco.neoloadFIXInitiatorLogonAction;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;

public class FIXInitiatorLogonActionTest {

	@Test
	public void shouldReturnType() {
		final FIXInitiatorAction action = new FIXInitiatorAction();
		assertEquals("FIXInitiatorLogon", action.getType());
		assertNotNull(action.getIcon());
	}

}
