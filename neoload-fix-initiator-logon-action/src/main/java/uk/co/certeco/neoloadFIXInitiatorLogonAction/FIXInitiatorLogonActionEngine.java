package uk.co.certeco.neoloadFIXInitiatorLogonAction;

import com.neotys.extensions.action.ActionParameter;
import com.neotys.extensions.action.engine.ActionEngine;
import com.neotys.extensions.action.engine.Context;
import com.neotys.extensions.action.engine.SampleResult;
import uk.co.alexcorrigan.simpleFIXApplication.fixApplication.FIXApplication;
import uk.co.certeco.neoloadFIXCommon.Constants;
import uk.co.certeco.neoloadFIXCommon.action.FIXActionType;
import uk.co.certeco.neoloadFIXCommon.actionEngine.FIXActionEngine;
import uk.co.certeco.neoloadFIXCommon.utils.FIXApplicationUtils;

import java.util.List;

import static uk.co.certeco.neoloadFIXCommon.utils.NeoLoadUtils.appendLineToStringBuilder;
import static uk.co.certeco.neoloadFIXCommon.utils.NeoLoadUtils.getErrorResult;

public final class FIXInitiatorLogonActionEngine extends FIXActionEngine {

    public FIXInitiatorLogonActionEngine() {
        super(FIXActionType.FIX_INITIATOR_LOGON_ACTION);
    }

    @Override
    protected void doMainExecution(Context context, List<ActionParameter> parameters, StringBuilder responseBuilder, StringBuilder requestBuilder) throws Exception {
        FIXInitiatorLogonActionParameters fixInitiatorLogonActionParameters = new FIXInitiatorLogonActionParameters(parameters);

        appendLineToStringBuilder(responseBuilder,"Set FIX Logon Action parameters");

        FIXApplication fixApplication = FIXApplication.initiatingFIXApplicationWithSimpleSessionHandler(
                fixInitiatorLogonActionParameters.buildFIXSessions(),
                fixInitiatorLogonActionParameters.getConnectHost(),
                fixInitiatorLogonActionParameters.getConnectPort());

        appendLineToStringBuilder(responseBuilder,"FIX Application created");

        fixApplication.start();

        appendLineToStringBuilder(responseBuilder,"FIX Application started, waiting for session to log on");

        FIXApplicationUtils.waitForSessionToLogon(responseBuilder, fixInitiatorLogonActionParameters.getSessionLogonTimeOut(), fixApplication);

        context.getCurrentVirtualUser().put(Constants.FIX_APPLICATION_CONTEXT_IDENTIFIER, fixApplication);
    }

}
