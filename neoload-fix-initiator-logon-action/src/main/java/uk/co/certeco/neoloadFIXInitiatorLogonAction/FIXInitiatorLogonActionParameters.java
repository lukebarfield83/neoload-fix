package uk.co.certeco.neoloadFIXInitiatorLogonAction;

import com.neotys.extensions.action.ActionParameter;
import uk.co.alexcorrigan.simpleFIXApplication.fixSession.FIXSessionSet;
import uk.co.alexcorrigan.simpleFIXApplication.fixSession.SimpleFIXSession;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FIXInitiatorLogonActionParameters {

    public static final String SENDER_COMP_ID_PARAMETER_NAME = "senderCompId";
    public static final String TARGET_COMP_ID_PARAMETER_NAME = "targetCompId";
    public static final String CONNECT_HOST_PARAMETER_NAME = "connectHost";
    public static final String CONNECT_PORT_PARAMETER_NAME = "connectPort";
    public static final String SESSION_LOGON_TIMEOUT_PARAMETER_NAME = "sessionLogonTimeout";
    public static final String BEGIN_STRING = "FIXT.1.1";

    private final String senderCompId;
    private final String targetCompId;
    private final String connectHost;
    private final String connectPort;
    private final long sessionLogonTimeOut;

    public FIXInitiatorLogonActionParameters(List<ActionParameter> actionParameters) throws Exception {

        Map<String, String> actionParameterMap = new HashMap<>();
        for (ActionParameter actionParameter : actionParameters) {
            actionParameterMap.put(actionParameter.getName(), actionParameter.getValue());
        }

        try {

            senderCompId = actionParameterMap.get(SENDER_COMP_ID_PARAMETER_NAME);
            targetCompId = actionParameterMap.get(TARGET_COMP_ID_PARAMETER_NAME);
            connectHost = actionParameterMap.get(CONNECT_HOST_PARAMETER_NAME);
            connectPort = actionParameterMap.get(CONNECT_PORT_PARAMETER_NAME);
            sessionLogonTimeOut = Long.valueOf(actionParameterMap.get(SESSION_LOGON_TIMEOUT_PARAMETER_NAME));

            validateStringParameter(SENDER_COMP_ID_PARAMETER_NAME, senderCompId);
            validateStringParameter(TARGET_COMP_ID_PARAMETER_NAME, targetCompId);
            validateStringParameter(CONNECT_HOST_PARAMETER_NAME, connectHost);
            validateStringParameter(CONNECT_PORT_PARAMETER_NAME, connectPort);
            validateLongParameter(SESSION_LOGON_TIMEOUT_PARAMETER_NAME, sessionLogonTimeOut);

        } catch (Exception e) {
            throw e;
        }

    }

    private void validateStringParameter(String parameterName, String parameterValue) throws Exception {
        if (parameterValue == null || parameterValue.isEmpty()) {
            throw new Exception(String.format("Value of %s parameter must not be empty", parameterName));
        }
    }

    private void validateLongParameter(String parameterName, Long parameterValue) throws Exception {

    }

    public FIXSessionSet buildFIXSessions() {
        FIXSessionSet fixSessions = new FIXSessionSet();
        fixSessions.addFIXSession(new SimpleFIXSession(senderCompId, BEGIN_STRING, senderCompId, targetCompId));
        return fixSessions;
    }

    public String getConnectHost() {
        return connectHost;
    }

    public String getConnectPort() {
        return connectPort;
    }

    public long getSessionLogonTimeOut() {
        return sessionLogonTimeOut;
    }

}
