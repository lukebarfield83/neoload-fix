package uk.co.certeco.neoloadFIXInitiatorLogonAction;

import com.neotys.extensions.action.ActionParameter;
import uk.co.certeco.neoloadFIXCommon.action.FIXActionType;
import uk.co.certeco.neoloadFIXCommon.action.FIXAction;

import java.util.ArrayList;
import java.util.List;

public final class FIXInitiatorAction extends FIXAction {

    public FIXInitiatorAction() {
        super(
                FIXActionType.FIX_INITIATOR_LOGON_ACTION,
                "FIX Initiator Logon",
                "Create FIX session and attempt logon.",
                FIXInitiatorLogonActionEngine.class,
                IconIdentifier.CONNECT);
    }

    @Override
    public List<ActionParameter> getDefaultActionParameters() {
        final List<ActionParameter> parameters = new ArrayList<ActionParameter>();
        parameters.add(new ActionParameter(FIXInitiatorLogonActionParameters.SENDER_COMP_ID_PARAMETER_NAME, "", ActionParameter.Type.TEXT));
        parameters.add(new ActionParameter(FIXInitiatorLogonActionParameters.TARGET_COMP_ID_PARAMETER_NAME, "", ActionParameter.Type.TEXT));
        parameters.add(new ActionParameter(FIXInitiatorLogonActionParameters.CONNECT_HOST_PARAMETER_NAME, "", ActionParameter.Type.TEXT));
        parameters.add(new ActionParameter(FIXInitiatorLogonActionParameters.CONNECT_PORT_PARAMETER_NAME, "", ActionParameter.Type.TEXT));
        parameters.add(new ActionParameter(FIXInitiatorLogonActionParameters.SESSION_LOGON_TIMEOUT_PARAMETER_NAME, "10000", ActionParameter.Type.TEXT));
        return parameters;
    }

}
