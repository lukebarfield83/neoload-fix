package uk.co.certeco.neoloadFIXLogoutAction;

import com.neotys.extensions.action.ActionParameter;
import uk.co.certeco.neoloadFIXCommon.action.FIXActionType;
import uk.co.certeco.neoloadFIXCommon.action.FIXAction;

import java.util.ArrayList;
import java.util.List;

public final class FIXLogoutAction extends FIXAction {

    public FIXLogoutAction() {
        super(
                FIXActionType.FIX_LOGOUT_ACTION,
                "FIX Logout Action",
                "Logout all FIX sessions and stop FIX application",
                FIXLogoutActionEngine.class,
                IconIdentifier.DISCONNECT);
    }


    @Override
    public List<ActionParameter> getDefaultActionParameters() {
        return new ArrayList<>();
    }

}
