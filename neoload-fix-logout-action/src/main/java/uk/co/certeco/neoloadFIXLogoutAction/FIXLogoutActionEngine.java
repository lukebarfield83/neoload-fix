package uk.co.certeco.neoloadFIXLogoutAction;

import com.neotys.extensions.action.ActionParameter;
import com.neotys.extensions.action.engine.ActionEngine;
import com.neotys.extensions.action.engine.Context;
import com.neotys.extensions.action.engine.SampleResult;
import uk.co.alexcorrigan.simpleFIXApplication.fixApplication.FIXApplication;
import uk.co.certeco.neoloadFIXCommon.Constants;
import uk.co.certeco.neoloadFIXCommon.action.FIXActionType;
import uk.co.certeco.neoloadFIXCommon.actionEngine.FIXActionEngine;

import java.util.List;

import static uk.co.certeco.neoloadFIXCommon.utils.NeoLoadUtils.appendLineToStringBuilder;
import static uk.co.certeco.neoloadFIXCommon.utils.NeoLoadUtils.getErrorResult;

public final class FIXLogoutActionEngine extends FIXActionEngine {

    public FIXLogoutActionEngine() {
        super(FIXActionType.FIX_LOGOUT_ACTION);
    }

    @Override
    protected void doMainExecution(Context context, List<ActionParameter> parameters, StringBuilder responseBuilder, StringBuilder requestBuilder) throws Exception {
        FIXApplication fixApplication = (FIXApplication) context.getCurrentVirtualUser().get(Constants.FIX_APPLICATION_CONTEXT_IDENTIFIER);

        appendLineToStringBuilder(responseBuilder, "Got FIX application for context");

        boolean allSessionsLoggedOn = fixApplication.allSessionsLoggedOn();

        appendLineToStringBuilder(responseBuilder, "All sessions logged on still = " + allSessionsLoggedOn);

        fixApplication.stop();

        appendLineToStringBuilder(responseBuilder, "FIX application stopped");
    }

}
