package uk.co.certeco.neoloadFIXLogoutAction;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;

public class FIXLogoutActionTest {

	@Test
	public void shouldReturnType() {
		final FIXLogoutAction action = new FIXLogoutAction();
		assertEquals("FIXLogout", action.getType());
		assertNotNull(action.getIcon());
	}

}
