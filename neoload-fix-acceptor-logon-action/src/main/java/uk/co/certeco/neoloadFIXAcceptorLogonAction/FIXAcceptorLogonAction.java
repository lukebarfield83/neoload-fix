package uk.co.certeco.neoloadFIXAcceptorLogonAction;

import com.neotys.extensions.action.ActionParameter;
import uk.co.certeco.neoloadFIXCommon.action.FIXActionType;
import uk.co.certeco.neoloadFIXCommon.action.FIXAction;

import java.util.ArrayList;
import java.util.List;

public final class FIXAcceptorLogonAction extends FIXAction {

    public FIXAcceptorLogonAction() {
        super(
                FIXActionType.FIX_ACCEPTOR_LOGON_ACTION,
                "FIX Acceptor Logon",
                "Create FIX session and wait for remote logon.",
                FIXAcceptorLogonActionEngine.class,
                IconIdentifier.CONNECT);
    }

    @Override
    public List<ActionParameter> getDefaultActionParameters() {
        final List<ActionParameter> parameters = new ArrayList<ActionParameter>();
        parameters.add(new ActionParameter(FIXAcceptorLogonActionParameters.SENDER_COMP_ID_PARAMETER_NAME, "", ActionParameter.Type.TEXT));
        parameters.add(new ActionParameter(FIXAcceptorLogonActionParameters.TARGET_COMP_ID_PARAMETER_NAME, "", ActionParameter.Type.TEXT));
        parameters.add(new ActionParameter(FIXAcceptorLogonActionParameters.ACCEPT_PORT_PARAMETER_NAME, "", ActionParameter.Type.TEXT));
        parameters.add(new ActionParameter(FIXAcceptorLogonActionParameters.SESSION_LOGON_TIMEOUT_PARAMETER_NAME, "10000", ActionParameter.Type.TEXT));
        return parameters;
    }



}
