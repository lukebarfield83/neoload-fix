package uk.co.certeco.neoloadFIXAcceptorLogonAction;

import com.neotys.extensions.action.ActionParameter;
import com.neotys.extensions.action.engine.Context;
import uk.co.alexcorrigan.simpleFIXApplication.fixApplication.FIXApplication;
import uk.co.certeco.neoloadFIXCommon.Constants;
import uk.co.certeco.neoloadFIXCommon.action.FIXActionType;
import uk.co.certeco.neoloadFIXCommon.actionEngine.FIXActionEngine;

import java.util.List;

import static uk.co.certeco.neoloadFIXCommon.utils.FIXApplicationUtils.waitForSessionToLogon;
import static uk.co.certeco.neoloadFIXCommon.utils.NeoLoadUtils.appendLineToStringBuilder;

public final class FIXAcceptorLogonActionEngine extends FIXActionEngine {

    public FIXAcceptorLogonActionEngine() {
        super(FIXActionType.FIX_ACCEPTOR_LOGON_ACTION);
    }

    @Override
    protected void doMainExecution(Context context, List<ActionParameter> parameters, StringBuilder responseBuilder, StringBuilder requestBuilder) throws Exception {
        FIXAcceptorLogonActionParameters fixAcceptorLogonActionParameters = new FIXAcceptorLogonActionParameters(parameters);

        appendLineToStringBuilder(responseBuilder, "Set FIX Logon Action parameters");

        FIXApplication fixApplication = FIXApplication.acceptingFIXApplicationWithSimpleSessionHandler(
                fixAcceptorLogonActionParameters.buildFIXSessions(),
                fixAcceptorLogonActionParameters.getAcceptPort());

        appendLineToStringBuilder(responseBuilder, "FIX Application created");

        fixApplication.start();

        appendLineToStringBuilder(responseBuilder, "FIX Application started, waiting for session to log on");

        waitForSessionToLogon(responseBuilder, fixAcceptorLogonActionParameters.getSessionLogonTimeOut(), fixApplication);

        context.getCurrentVirtualUser().put(Constants.FIX_APPLICATION_CONTEXT_IDENTIFIER, fixApplication);
    }

}
