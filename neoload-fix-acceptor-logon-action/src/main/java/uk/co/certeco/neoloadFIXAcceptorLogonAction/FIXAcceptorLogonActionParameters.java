package uk.co.certeco.neoloadFIXAcceptorLogonAction;

import com.neotys.extensions.action.ActionParameter;
import uk.co.alexcorrigan.simpleFIXApplication.fixSession.FIXSessionSet;
import uk.co.alexcorrigan.simpleFIXApplication.fixSession.SimpleFIXSession;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FIXAcceptorLogonActionParameters {

    public static final String SENDER_COMP_ID_PARAMETER_NAME = "senderCompId";
    public static final String TARGET_COMP_ID_PARAMETER_NAME = "targetCompId";
    public static final String ACCEPT_PORT_PARAMETER_NAME = "acceptPort";
    public static final String SESSION_LOGON_TIMEOUT_PARAMETER_NAME = "sessionLogonTimeout";
    public static final String BEGIN_STRING = "FIXT.1.1";

    private final String senderCompId;
    private final String targetCompId;
    private final String acceptPort;
    private final long sessionLogonTimeOut;

    public FIXAcceptorLogonActionParameters(List<ActionParameter> actionParameters) throws Exception {

        Map<String, String> actionParameterMap = new HashMap<>();
        for (ActionParameter actionParameter : actionParameters) {
            actionParameterMap.put(actionParameter.getName(), actionParameter.getValue());
        }

        try {

            senderCompId = actionParameterMap.get(SENDER_COMP_ID_PARAMETER_NAME);
            targetCompId = actionParameterMap.get(TARGET_COMP_ID_PARAMETER_NAME);
            acceptPort = actionParameterMap.get(ACCEPT_PORT_PARAMETER_NAME);
            sessionLogonTimeOut = Long.valueOf(actionParameterMap.get(SESSION_LOGON_TIMEOUT_PARAMETER_NAME));

            validateStringParameter(SENDER_COMP_ID_PARAMETER_NAME, senderCompId);
            validateStringParameter(TARGET_COMP_ID_PARAMETER_NAME, targetCompId);
            validateStringParameter(ACCEPT_PORT_PARAMETER_NAME, acceptPort);
            validateLongParameter(SESSION_LOGON_TIMEOUT_PARAMETER_NAME, sessionLogonTimeOut);

        } catch (Exception e) {
            throw e;
        }

    }

    private void validateStringParameter(String parameterName, String parameterValue) throws Exception {
        if (parameterValue == null || parameterValue.isEmpty()) {
            throw new Exception(String.format("Value of %s parameter must not be empty", parameterName));
        }
    }

    private void validateLongParameter(String parameterName, Long parameterValue) throws Exception {

    }

    public FIXSessionSet buildFIXSessions() {
        FIXSessionSet fixSessions = new FIXSessionSet();
        fixSessions.addFIXSession(new SimpleFIXSession(senderCompId, BEGIN_STRING, senderCompId, targetCompId));
        return fixSessions;
    }

    public String getAcceptPort() {
        return acceptPort;
    }

    public long getSessionLogonTimeOut() {
        return sessionLogonTimeOut;
    }

}
