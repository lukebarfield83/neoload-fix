package uk.co.certeco.neoloadFIXAcceptorLogonAction;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;

public class FIXAcceptorLogonActionTest {

	@Test
	public void shouldReturnType() {
		final FIXAcceptorLogonAction action = new FIXAcceptorLogonAction();
		assertEquals("FIXAcceptorLogon", action.getType());
		assertNotNull(action.getIcon());
	}

}
