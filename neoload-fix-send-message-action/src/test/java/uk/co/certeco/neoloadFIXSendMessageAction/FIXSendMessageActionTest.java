package uk.co.certeco.neoloadFIXSendMessageAction;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;

public class FIXSendMessageActionTest {

	@Test
	public void shouldReturnType() {
		final FIXSendMessageAction action = new FIXSendMessageAction();
		assertEquals("FIXSendMessage", action.getType());
		assertNotNull(action.getIcon());
	}

}
