package uk.co.certeco.neoloadFIXSendMessageAction;

import com.neotys.extensions.action.ActionParameter;
import uk.co.certeco.neoloadFIXCommon.action.FIXActionType;
import uk.co.certeco.neoloadFIXCommon.action.FIXAction;

import java.util.ArrayList;
import java.util.List;

import static uk.co.certeco.neoloadFIXSendMessageAction.FIXSendMessageActionParameters.FIX_MESSAGE_STRING;
import static uk.co.certeco.neoloadFIXSendMessageAction.FIXSendMessageActionParameters.SENDER_COMP_ID_PARAMETER;

public final class FIXSendMessageAction extends FIXAction {

    public FIXSendMessageAction() {
        super(
                FIXActionType.FIX_SEND_MESSAGE_ACTION,
                "FIX Send Message",
                "Send message on active session.",
                FIXSendMessageActionEngine.class,
                IconIdentifier.MESSAGE);
    }

    @Override
    public List<ActionParameter> getDefaultActionParameters() {
        final List<ActionParameter> parameters = new ArrayList<ActionParameter>();
        parameters.add(new ActionParameter(SENDER_COMP_ID_PARAMETER, "", ActionParameter.Type.TEXT));
        parameters.add(new ActionParameter(FIX_MESSAGE_STRING, "", ActionParameter.Type.TEXT));
        return parameters;
    }

}
