package uk.co.certeco.neoloadFIXSendMessageAction;

import com.neotys.extensions.action.ActionParameter;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FIXSendMessageActionParameters {

    public static final String SENDER_COMP_ID_PARAMETER = "senderCompId";
    public static final String FIX_MESSAGE_STRING = "fixMessageString";

    private String senderCompId;
    private String fixMessageString;

    public FIXSendMessageActionParameters(List<ActionParameter> actionParameters) throws Exception {

        Map<String, String> actionParameterMap = new HashMap<>();
        for (ActionParameter actionParameter : actionParameters) {
            actionParameterMap.put(actionParameter.getName(), actionParameter.getValue());
        }

        try {

            senderCompId = actionParameterMap.get(SENDER_COMP_ID_PARAMETER);
            fixMessageString = actionParameterMap.get(FIX_MESSAGE_STRING);

            validateStringParameter(SENDER_COMP_ID_PARAMETER, senderCompId);
            validateStringParameter(FIX_MESSAGE_STRING, fixMessageString);

        } catch (Exception e) {
            throw e;
        }

    }

    private void validateStringParameter(String parameterName, String parameterValue) throws Exception {
        if (parameterValue == null || parameterValue.isEmpty()) {
            throw new Exception(String.format("Value of %s parameter must not be empty", parameterName));
        }
    }

    public String getSenderCompId() {
        return senderCompId;
    }

    public String getFixMessageString() {
        return fixMessageString;
    }

}
