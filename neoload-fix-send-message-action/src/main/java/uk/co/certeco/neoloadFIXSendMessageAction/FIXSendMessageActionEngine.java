package uk.co.certeco.neoloadFIXSendMessageAction;

import com.neotys.extensions.action.ActionParameter;
import com.neotys.extensions.action.engine.ActionEngine;
import com.neotys.extensions.action.engine.Context;
import com.neotys.extensions.action.engine.SampleResult;
import uk.co.alexcorrigan.simpleFIXApplication.fixApplication.FIXApplication;
import uk.co.certeco.neoloadFIXCommon.Constants;
import uk.co.certeco.neoloadFIXCommon.action.FIXActionType;
import uk.co.certeco.neoloadFIXCommon.actionEngine.FIXActionEngine;

import java.util.List;

import static uk.co.certeco.neoloadFIXCommon.utils.NeoLoadUtils.appendLineToStringBuilder;
import static uk.co.certeco.neoloadFIXCommon.utils.NeoLoadUtils.getErrorResult;

public final class FIXSendMessageActionEngine extends FIXActionEngine {

    public FIXSendMessageActionEngine() {
        super(FIXActionType.FIX_SEND_MESSAGE_ACTION);
    }

    @Override
    protected void doMainExecution(
            Context context,
            List<ActionParameter> parameters,
            StringBuilder responseBuilder,
            StringBuilder requestBuilder) throws Exception {

        FIXSendMessageActionParameters fixSendMessageActionParameters = new FIXSendMessageActionParameters(parameters);

        FIXApplication fixApplication = (FIXApplication) context.getCurrentVirtualUser().get(Constants.FIX_APPLICATION_CONTEXT_IDENTIFIER);

        appendLineToStringBuilder(responseBuilder, "Got FIX application for context");



        fixApplication.sendFIXMessageString(
                fixSendMessageActionParameters.getSenderCompId(),
                fixSendMessageActionParameters.getFixMessageString());

        context.getCurrentVirtualUser().put("fixApplication", fixApplication);

    }

}
