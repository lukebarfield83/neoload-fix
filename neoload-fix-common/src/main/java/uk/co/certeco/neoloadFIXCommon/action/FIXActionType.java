package uk.co.certeco.neoloadFIXCommon.action;

public enum FIXActionType {

    FIX_ACCEPTOR_LOGON_ACTION("FIXAcceptorLogon"),
    FIX_INITIATOR_LOGON_ACTION("FIXInitiatorLogon"),
    FIX_SEND_MESSAGE_ACTION("FIXSendMessage"),
    FIX_LOGOUT_ACTION("FIXLogout");

    private String name;

    FIXActionType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

}
