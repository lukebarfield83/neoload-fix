package uk.co.certeco.neoloadFIXCommon.actionEngine;

import com.neotys.extensions.action.ActionParameter;
import com.neotys.extensions.action.engine.ActionEngine;
import com.neotys.extensions.action.engine.Context;
import com.neotys.extensions.action.engine.SampleResult;
import uk.co.certeco.neoloadFIXCommon.action.FIXActionType;

import java.util.List;

import static uk.co.certeco.neoloadFIXCommon.utils.NeoLoadUtils.getErrorResult;

public abstract class FIXActionEngine implements ActionEngine {

    private final FIXActionType fixActionType;

    public FIXActionEngine(FIXActionType fixActionType) {
        this.fixActionType = fixActionType;
    }

    @Override
    public SampleResult execute(Context context, List<ActionParameter> list) {
        final SampleResult sampleResult = new SampleResult();

        try {
            sampleResult.sampleStart();

            StringBuilder requestBuilder = new StringBuilder();
            StringBuilder responseBuilder = new StringBuilder();

            doMainExecution(context, list, responseBuilder, requestBuilder);

            sampleResult.sampleEnd();

            sampleResult.setRequestContent(requestBuilder.toString());
            sampleResult.setResponseContent(responseBuilder.toString());
            sampleResult.setStatusCode("NL-" + fixActionType.getName() + "_SUCCESS");

            return sampleResult;

        } catch (Exception e) {
            return getErrorResult(fixActionType, context, sampleResult, e.getMessage(), e);
        }


    }

    protected abstract void doMainExecution(
            Context context,
            List<ActionParameter> parameters,
            StringBuilder responseBuilder,
            StringBuilder requestBuilder) throws Exception;

    @Override
    public void stopExecute() {}

}
