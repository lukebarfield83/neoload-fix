package uk.co.certeco.neoloadFIXCommon.action;

import com.google.common.base.Optional;
import com.neotys.extensions.action.Action;
import com.neotys.extensions.action.ActionParameter;
import com.neotys.extensions.action.engine.ActionEngine;
import uk.co.certeco.neoloadFIXCommon.actionEngine.FIXActionEngine;

import javax.swing.*;
import java.util.List;

public abstract class FIXAction implements Action {

    private static final String ICON_ROOT = "/icons/";

    public enum IconIdentifier {
        CONNECT(ICON_ROOT + "connection.png"),
        DISCONNECT(ICON_ROOT + "disconnect.png"),
        MESSAGE(ICON_ROOT + "message.png"),;

        private String iconPath;

        IconIdentifier(String iconPath) {
            this.iconPath = iconPath;
        }

        public String getIconPath() {
            return iconPath;
        }
    }

    private static final String DISPLAY_PATH = "Messaging/FIX";

    private FIXActionType fixActionType;
    private String displayName;
    private String description;
    private Class<? extends ActionEngine> fixActionEngineClass;
    private Icon icon;

    public FIXAction(
            FIXActionType fixActionType,
            String displayName,
            String description,
            Class<? extends FIXActionEngine> fixActionEngineClass,
            IconIdentifier iconIdentifier) {
        this.fixActionType = fixActionType;
        this.displayName = displayName;
        this.description = description;
        this.fixActionEngineClass = fixActionEngineClass;
        icon = new ImageIcon(FIXAction.class.getResource(iconIdentifier.getIconPath()));
    }

    @Override
    public String getType() {
        return fixActionType.getName();
    }

    @Override
    public abstract List<ActionParameter> getDefaultActionParameters();

    @Override
    public Class<? extends ActionEngine> getEngineClass() {
        return fixActionEngineClass;
    }

    @Override
    public Icon getIcon() {
        return icon;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public String getDisplayName() {
        return displayName;
    }

    @Override
    public String getDisplayPath() {
        return DISPLAY_PATH;
    }

    @Override
    public Optional<String> getMinimumNeoLoadVersion() {
        return Optional.absent();
    }

    @Override
    public Optional<String> getMaximumNeoLoadVersion() {
        return Optional.absent();
    }

}
