package uk.co.certeco.neoloadFIXCommon.utils;

import uk.co.alexcorrigan.simpleFIXApplication.fixApplication.FIXApplication;

public class FIXApplicationUtils {

    public static void waitForSessionToLogon(StringBuilder responseBuilder, long logonTimeout, FIXApplication fixApplication) throws Exception {
        long logonTimerStart = System.currentTimeMillis();
        boolean allSessionsLoggedOn = false;

        while (!allSessionsLoggedOn && System.currentTimeMillis() - logonTimerStart < logonTimeout) {
            Thread.sleep(500);
            allSessionsLoggedOn = fixApplication.allSessionsLoggedOn();
        }

        if (allSessionsLoggedOn) {
            long timeForSessionsToLogon = System.currentTimeMillis() - logonTimerStart;
            NeoLoadUtils.appendLineToStringBuilder(responseBuilder,"All sessions logged on in " + timeForSessionsToLogon + "ms");
        } else {
            throw new Exception("Not all sessions logged on in time");
        }
    }

}
