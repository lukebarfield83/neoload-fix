package uk.co.certeco.neoloadFIXCommon.utils;

import com.neotys.extensions.action.engine.Context;
import com.neotys.extensions.action.engine.SampleResult;
import uk.co.certeco.neoloadFIXCommon.action.FIXActionType;

public class NeoLoadUtils {

    public static void appendLineToStringBuilder(final StringBuilder sb, final String line){
        sb.append(line).append("\n");
    }

    public static SampleResult getErrorResult(
            FIXActionType fixActionType,
            Context context,
            SampleResult result,
            String errorMessage,
            Exception exception) {
        result.setError(true);
        result.setStatusCode("NL-" + fixActionType.getName() + "_ERROR");
        result.setResponseContent(errorMessage);
        if(exception != null){
            context.getLogger().error(errorMessage, exception);
        } else{
            context.getLogger().error(errorMessage);
        }
        return result;
    }

}
